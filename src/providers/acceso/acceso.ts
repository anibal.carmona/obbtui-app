import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { GlobalVar } from '../../config';

@Injectable()
export class AccesoProvider {
  public url: string;

  constructor(public http: HttpClient) {
    let env = this;
    env.url = GlobalVar.BASE_API_URL + 'acceso/';
  }

  acceder(run: string, contrasena: string, dispositivo: string): Observable<any> {
    let env = this;
    let arg = { run: run, contrasena: contrasena, dispositivo: dispositivo };
    return env.http
      .post(env.url, { arg: JSON.stringify(arg) })
      .map(function(response) { return response })
      .catch(env.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Ha ocurrido un error', error);
    return Promise.reject(error.message || error);
  }
}